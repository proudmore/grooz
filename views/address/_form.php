<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Address */
/* @var $form yii\widgets\ActiveForm */


\app\assets\SuggestionAsset::register($this);

$js = <<<JS
$(document).ready(function(){
    
    var el = document.getElementById('address-address');
    
    var autocomplete = new google.maps.places.Autocomplete(el, {types: ['geocode']});
});
JS;

$this->registerJs($js);


?>

<div class="address-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_id')->widget(\kartik\select2\Select2::className(), [
            'pluginOptions' => [
                'allowClear' => false,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new \yii\web\JsExpression("function () { return 'Ожидание загрузки...'; }"),
                ],
                'ajax' => [
                    'url' => '/customer/seek',
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
    ]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
