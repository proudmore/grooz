<?php

namespace app\models;

use app\models\behaviors\DateFormatTranslator;
use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $birth_date
 * @property int $gender
 * @property string $phone
 *
 * @property Address[] $addresses
 */
class Customer extends \yii\db\ActiveRecord
{


    const MALE_GENDER = 0;
    const FEMALE_GENDER = 1;

    static $genders = [
        self::MALE_GENDER => 'Мужской',
        self::FEMALE_GENDER => 'Женский'
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translator' => [
                'class' => DateFormatTranslator::className(),
                'attributes' => ['birth_date']
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birth_date', 'name', 'surname', 'phone'], 'required'],
            [['birth_date'], 'date', 'format' => 'php:d.m.Y'],
            [['gender'], 'boolean'],
            [['name', 'surname', 'phone'], 'string', 'max' => 255],
            [
                'phone',
                'match',
                'pattern' => '/^\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/',
                'message' => 'Пожалуйста, введите телефон в формате +7 (777) 777-7777'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'birth_date' => 'Дата рождения',
            'gender' => 'Пол',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['customer_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\CustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\CustomerQuery(get_called_class());
    }
}
