<?php

use yii\db\Migration;

/**
 * Class m180215_045932_add_tables_customer_and_address
 */
class m180215_045932_add_tables_customer_and_address extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $tableOptions = null;

        if(Yii::$app->db->driverName == 'mysql'){
            $tableOptions = 'CHARACTER SET utf8 ENGINE=InnoDB';
        }

        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'surname' => $this->string(),
            'birth_date' => $this->date(),
            'gender' => $this->boolean()->defaultValue(0),
            'phone' => $this->string()
        ], $tableOptions);

        $this->createTable('address', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'address' => $this->string()->notNull()
        ], $tableOptions);

        $this->addForeignKey(
            'ADDR_FK',
            'address',
            'customer_id',
            'customer',
            'id'
        );

        $this->createIndex('ADDR_NAME_IDX', 'address', 'name');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('address');
        $this->dropTable('customer');
    }


}
