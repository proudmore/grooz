<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $addrProvider \yii\data\ActiveDataProvider */

$this->title = $model->surname;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот объект?',
                'method' => 'post',
            ]
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'birth_date',
            [
                'attribute' => 'gender',
                'value' => function($model){
                    return \app\models\Customer::$genders[$model->gender];
                },
            ],
            'phone',
        ],
    ]) ?>

    <h3>Адреса</h3>

    <p>
        <?= Html::a('Создать адрес', ['address/create', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php \yii\widgets\Pjax::begin(); ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $addrProvider,
        'columns' => [
            'name',
            'address',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'urlCreator' => function ($action, $model) {
                    return Url::to(["/address/{$action}", 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>
