<?php

namespace app\assets;


use yii\web\AssetBundle;

class SuggestionAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        "https://maps.googleapis.com/maps/api/js?key=AIzaSyB7AmJEvbeYeXI0IWowu4CKC-MpmqhR49Q&libraries=places"
    ];
}